//
//  IntentHandler.swift
//  IntentsExtension
//
//  Created by Bohdan Korda on 28.08.2020.
//  Copyright © 2020 Bohdan Korda. All rights reserved.
//

import Intents

// As an example, this class is set up to handle Message intents.
// You will want to replace this or add other intents as appropriate.
// The intents you wish to handle must be declared in the extension's Info.plist.

// You can test your example integration by saying things to Siri like:
// "Send a message using <myApp>"
// "<myApp> John saying hello"
// "Search for messages in <myApp>"

//extension INPerson {
//    convenience init(contact: FetchedContact) {
//
//    }
//}

class IntentHandler: INExtension {
    override func handler(for intent: INIntent) -> Any? {
        switch intent {
        case is WhereIsMyMoneyIntent:
            return WhereIsMyMoneyIntentHandler()
        default:
            return self
        }
    }
}

extension IntentHandler: INSendPaymentIntentHandling {
    func handle(intent: INSendPaymentIntent, completion: @escaping (INSendPaymentIntentResponse) -> Void) {
        
        guard let amount = intent.currencyAmount?.amount?.doubleValue else {
            completion(INSendPaymentIntentResponse(code: .failure, userActivity: nil))
            return
        }
        BankAccount.shared.withdraw(value: amount)
        completion(INSendPaymentIntentResponse(code: .success, userActivity: nil))
    }
    
    func resolveCurrencyAmount(for intent: INSendPaymentIntent, with completion: @escaping (INSendPaymentCurrencyAmountResolutionResult) -> Void) {
        if let amount = intent.currencyAmount {
            completion (
                INSendPaymentCurrencyAmountResolutionResult.success(with: amount)
            )
        } else {
            completion (
                INSendPaymentCurrencyAmountResolutionResult.needsValue()
            )
        }
    }
    
    func resolveNote(for intent: INSendPaymentIntent, with completion: @escaping (INStringResolutionResult) -> Void) {
        guard let note = intent.note else {
            completion(INStringResolutionResult.needsValue())
            return
        }
        completion(INStringResolutionResult.success(with: note))
    }

    func resolvePayee(for intent: INSendPaymentIntent, with completion: @escaping (INPersonResolutionResult) -> Void) {
        
        if let payee = intent.payee {
                // Look up contacts that match the payee.
            ContactFetcher.shared.lookup(displayName: payee.displayName) { contacts in
                    // Build the `INIntentResolutionResult` to pass to the `completion` closure.
                    let result: INPersonResolutionResult

                    if let contact = contacts.first, contacts.count == 1 {
                        // An exact single match.
                        let resolvedPayee = INPerson(contact: contact)
                        result = INPersonResolutionResult.success(with: resolvedPayee)
                    } else if contacts.isEmpty {
                        // Found no matches.
                        result = INPersonResolutionResult.unsupported()
                    } else {
                        // Found more than one match; user needs to clarify the intended contact.
                        let people: [INPerson] = contacts.map { contact in
                            return INPerson(contact: contact)
                        }
                        result = INPersonResolutionResult.disambiguation(with: people)
                    }
                    completion(result)
                }
            } /*else if let mostRecentPayee = paymentProvider.mostRecentPayment?.contact {
                // No payee provided; suggest the last payee.
                let result = INPersonResolutionResult.confirmationRequired(with: INPerson(contact: mostRecentPayee))
                completion(result)
            }*/ else {
                // No payee provided and there was no previous payee.
                let result = INPersonResolutionResult.needsValue()
                completion(result)
            }
    }
    
    func confirm(intent: INSendPaymentIntent, completion: @escaping (INSendPaymentIntentResponse) -> Void) {
        let response = INSendPaymentIntentResponse(code: .ready, userActivity: nil)
        response.paymentRecord = INPaymentRecord(payee: intent.payee, payer: nil, currencyAmount: intent.currencyAmount, paymentMethod: nil, note: intent.note, status: .pending)
        completion(response)
    }
}

extension IntentHandler: INRequestPaymentIntentHandling {
    func handle(intent: INRequestPaymentIntent, completion: @escaping (INRequestPaymentIntentResponse) -> Void) {
        
        guard let amount = intent.currencyAmount?.amount?.doubleValue else {
            completion(INRequestPaymentIntentResponse(code: .failure, userActivity: nil))
            return
        }
        BankAccount.shared.deposit(value: amount)
        completion(INRequestPaymentIntentResponse(code: .success, userActivity: nil))
    }
    
    func resolveCurrencyAmount(for intent: INRequestPaymentIntent, with completion: @escaping (INRequestPaymentCurrencyAmountResolutionResult) -> Void) {
        
        let amount = intent.currencyAmount?.amount ?? 0
               
        completion (
            INRequestPaymentCurrencyAmountResolutionResult.success(with: INCurrencyAmount(amount: amount, currencyCode: "USD"))
        )
    }
    
    func resolveNote(for intent: INRequestPaymentIntent, with completion: @escaping (INStringResolutionResult) -> Void) {
        guard let note = intent.note else {
            completion(INStringResolutionResult.needsValue())
            return
        }
        completion(INStringResolutionResult.success(with: note))
    }
    
    func resolvePayer(for intent: INRequestPaymentIntent, with completion: @escaping (INRequestPaymentPayerResolutionResult) -> Void) {
        guard let payer = intent.payer else {
            completion(INRequestPaymentPayerResolutionResult.needsValue())
            return
        }
        completion(INRequestPaymentPayerResolutionResult.success(with: payer))
    }
    
    func confirm(intent: INRequestPaymentIntent, completion: @escaping (INRequestPaymentIntentResponse) -> Void) {
        let response = INRequestPaymentIntentResponse(code: .ready, userActivity: nil)
        response.paymentRecord = INPaymentRecord(payee: nil, payer: intent.payer, currencyAmount: intent.currencyAmount, paymentMethod: nil, note: intent.note, status: .pending)
        completion(response)
    }
}

extension IntentHandler : INPayBillIntentHandling {
    func resolveBillPayee(for intent: INPayBillIntent, with completion: @escaping (INBillPayeeResolutionResult) -> Void) {
//        guard let payee = intent.billPayee else {
//            completion(INBillPayeeResolutionResult.needsValue())
//            return
//        }
//
//        print(payee.debugDescription)
//        completion(.success(with: payee))
        if let payee = intent.billPayee {
                // Look up contacts that match the payee.
            ContactFetcher.shared.lookup(displayName: payee.nickname!.spokenPhrase ) { contacts in
                    // Build the `INIntentResolutionResult` to pass to the `completion` closure.
                    let result: INBillPayeeResolutionResult

                    if let contact = contacts.first, contacts.count == 1 {
                        // An exact single match.
                        let resolvedPayee = INBillPayee(contact: contact)
                        result = INBillPayeeResolutionResult.success(with: resolvedPayee!)
                    } else if contacts.isEmpty {
                        // Found no matches.
                        result = INBillPayeeResolutionResult.unsupported()
                    } else {
                        // Found more than one match; user needs to clarify the intended contact.
                        let people: [INBillPayee] = contacts.map { contact in
                            return INBillPayee(contact: contact)!
                        }
                        result = INBillPayeeResolutionResult.disambiguation(with: people)
                    }
                    completion(result)
                }
            } /*else if let mostRecentPayee = paymentProvider.mostRecentPayment?.contact {
                // No payee provided; suggest the last payee.
                let result = INPersonResolutionResult.confirmationRequired(with: INPerson(contact: mostRecentPayee))
                completion(result)
            }*/ else {
                // No payee provided and there was no previous payee.
                let result = INBillPayeeResolutionResult.needsValue()
                completion(result)
            }
    }

    
    func resolveFromAccount(for intent: INPayBillIntent, with completion: @escaping (INPaymentAccountResolutionResult) -> Void) {
        guard let fromAccount = intent.fromAccount else {
            completion(INPaymentAccountResolutionResult.needsValue())
            return
        }
        
        completion(.success(with: fromAccount))
    }

    
    func resolveTransactionAmount(for intent: INPayBillIntent, with completion: @escaping (INPaymentAmountResolutionResult) -> Void) {
        
        guard let amount = intent.transactionAmount else {
            completion(INPaymentAmountResolutionResult.needsValue())
            return
        }
        
        completion(.success(with: amount))
    }
    
    func resolveBillType(for intent: INPayBillIntent, with completion: @escaping (INBillTypeResolutionResult) -> Void) {
        switch intent.billType {
        case .phone:
            completion(.success(with: .phone))
        default:
            completion(.confirmationRequired(with: .phone))
        }
    }
    
    func confirm(intent: INPayBillIntent, completion: @escaping (INPayBillIntentResponse) -> Void) {
        let response = INPayBillIntentResponse(code: .ready, userActivity: nil)
        let fee = INCurrencyAmount(amount: NSDecimalNumber(value: 1), currencyCode: "USD")
        
        let payee = INBillPayee(nickname: INSpeakableString(spokenPhrase: "Богдан Корда"), number: "0971240145", organizationName: INSpeakableString(spokenPhrase: "Приватбанк"))
        response.billDetails = INBillDetails(billType: intent.billType, paymentStatus: .pending, billPayee: payee, amountDue: nil, minimumDue: nil, lateFee: fee, dueDate: nil, paymentDate: nil)
        completion(response)
    }
    
    func handle(intent: INPayBillIntent, completion: @escaping (INPayBillIntentResponse) -> Void) {
        completion(INPayBillIntentResponse(code: .success, userActivity: nil))
    }
}
