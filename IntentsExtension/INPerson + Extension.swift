//
//  INPerson + Extension.swift
//  IntentsExtension
//
//  Created by Bohdan Korda on 25.09.2020.
//  Copyright © 2020 Bohdan Korda. All rights reserved.
//

import Intents

extension INPerson {
    convenience init(contact: FetchedContact, isMe: Bool = false) {
        let handle = INPersonHandle(value: contact.telephone, type: .phoneNumber, label: INPersonHandleLabel(rawValue: "home"))
        
        var nameComponents = PersonNameComponents()
        nameComponents.givenName = contact.firstName
        nameComponents.familyName = contact.lastName
        
        let displayName = "\(contact.firstName) \(contact.lastName)"
        
        self.init(personHandle: handle, nameComponents: nameComponents, displayName: displayName, image: nil, contactIdentifier: contact.identifier, customIdentifier: nil, isMe: isMe)
    }
}

extension INBillPayee {
    convenience init?(contact: FetchedContact, isMe: Bool = false) {
        let displayName = "\(contact.firstName) \(contact.lastName)"
        self.init(nickname: INSpeakableString(spokenPhrase: displayName), number: contact.telephone, organizationName: nil)
    }
}
