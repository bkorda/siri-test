//
//  WhereIsMyMoneyIntentHandler.swift
//  Siri Bucks
//
//  Created by Bohdan Korda on 31.08.2020.
//  Copyright © 2020 Bohdan Korda. All rights reserved.
//

import Foundation
import Intents

class WhereIsMyMoneyIntentHandler : NSObject, WhereIsMyMoneyIntentHandling {
    
    func confirm(intent: WhereIsMyMoneyIntent, completion: @escaping (WhereIsMyMoneyIntentResponse) -> Void) {
        guard let amount = intent.amount else {
            completion(WhereIsMyMoneyIntentResponse.init(code: .failure, userActivity: nil))
            return
        }
        let respsonse = WhereIsMyMoneyIntentResponse.init(code: .success, userActivity: nil)
        respsonse.amount = amount
        completion(respsonse)
    }
    
    func resolveAmount(for intent: WhereIsMyMoneyIntent, with completion: @escaping (WhereIsMyMoneyAmountResolutionResult) -> Void) {
        guard let amount = intent.amount else {
            completion(WhereIsMyMoneyAmountResolutionResult.needsValue())
            return
        }

        completion(WhereIsMyMoneyAmountResolutionResult.success(with: amount))
    }
    
    func handle(intent: WhereIsMyMoneyIntent, completion: @escaping (WhereIsMyMoneyIntentResponse) -> Void) {
        guard let amount = intent.amount else {
            completion(WhereIsMyMoneyIntentResponse.init(code: .failure, userActivity: nil))
            return
        }
        let respsonse = WhereIsMyMoneyIntentResponse.init(code: .success, userActivity: nil)
        respsonse.amount = amount
        
        BankAccount.shared.deposit(value: amount.amount!.doubleValue)
        
        completion(respsonse)
    }
}
