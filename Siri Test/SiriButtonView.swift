//
//  SiriButtonView.swift
//  Siri Bucks
//
//  Created by Bohdan Korda on 31.08.2020.
//  Copyright © 2020 Bohdan Korda. All rights reserved.
//

import SwiftUI

struct SiriButtonView: UIViewControllerRepresentable {
    var shortcut: ShortcutManager.Shortcut
    
    func makeUIViewController(context: Context) -> SiriShortcutViewController {
        let controller = SiriShortcutViewController()
        controller.shortcut = shortcut
        return controller
    }
    
    func updateUIViewController(_ uiViewController: SiriShortcutViewController, context: Context) {
        
    }
}

struct SiriButtonView_Previews: PreviewProvider {
    static var previews: some View {
        SiriButtonView(shortcut: .whereIsMoney)
    }
}
