//
//  ContactFetcher.swift
//  Siri Bucks
//
//  Created by Bohdan Korda on 23.09.2020.
//  Copyright © 2020 Bohdan Korda. All rights reserved.
//

import Foundation
import Contacts

struct FetchedContact {
    var firstName: String
    var lastName: String
    var telephone: String
    var identifier: String
}

class ContactFetcher {
    
    static let shared = ContactFetcher()
    
    var contacts: [FetchedContact] = []
    
    init() {
        fetchContacts()
    }
    
    func fetchContacts() {
        // 1.
        let store = CNContactStore()
        
        store.requestAccess(for: .contacts) { [weak self] (granted, error) in
            if let error = error {
                print("failed to request access", error)
                return
            }
            if granted {
                let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
                let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
                do {
                    // 3.
                    try store.enumerateContacts(with: request, usingBlock: { (contact, stopPointer) in
                        self?.contacts.append(FetchedContact(firstName: contact.givenName, lastName: contact.familyName, telephone: contact.phoneNumbers.first?.value.stringValue ?? "", identifier: contact.identifier))
                    })
                } catch let error {
                    print("Failed to enumerate contact", error)
                }
            } else {
                print("access denied")
            }
        }
    }
    
    func lookup(displayName: String, completion:([FetchedContact]) -> Void) {
        let foundContacts = contacts.filter { contact -> Bool in
            let combinedName = "\(contact.firstName) \(contact.lastName)"
            return combinedName.contains(displayName)
        }
        completion(foundContacts)
    }
}
