//
//  PermissionManager.swift
//  Siri Bucks
//
//  Created by Bohdan Korda on 28.08.2020.
//  Copyright © 2020 Bohdan Korda. All rights reserved.
//

import Foundation
import Intents

struct PermissionManager {
    func askSiri() {
        INPreferences.requestSiriAuthorization { (status) in
            
        }
    }
    
    func askContacts() {
        ContactFetcher.shared.fetchContacts()
    }
}
