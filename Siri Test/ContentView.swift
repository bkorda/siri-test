//
//  ContentView.swift
//  Siri Test
//
//  Created by Bohdan Korda on 27.08.2020.
//  Copyright © 2020 Bohdan Korda. All rights reserved.
//

import SwiftUI
import Intents

struct ContentView: View {
    
    @ObservedObject var bankAccount = BankAccount.shared
    
    var body: some View {
        VStack {
            Text("Siri bucks")
                .font(.title)
            Text("Account ballance " + "\(bankAccount.balance)")
            Button(action: {
                self.bankAccount.checkBalance()
            }) {
                Text("Check balance")
            }.padding()
            
            SiriButtonView(shortcut: .whereIsMoney)
                .frame(width: 150, height: 20, alignment: .center)
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
