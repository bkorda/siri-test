//
//  BankAccount.swift
//  Siri Test
//
//  Created by Bohdan Korda on 27.08.2020.
//  Copyright © 2020 Bohdan Korda. All rights reserved.
//

import Foundation

class BankAccount: ObservableObject {
    
    enum UserDefaultsKeys: String {
        case balance
    }
    
    private let suiteName = "group.ua.privatbank.SiriTest"
    private let permissionManager = PermissionManager()
    
    public static var shared = BankAccount()
    
    @Published private(set) var balance: Double = 1000 {
        didSet {            
            UserDefaults(suiteName: suiteName).map { $0.set(balance, forKey: UserDefaultsKeys.balance.rawValue) }
        }
    }
    
    private init() {
        permissionManager.askSiri()
        permissionManager.askContacts()
        
        ContactFetcher.shared.lookup(displayName: "Артур Люкасон") { contacts in
            print(contacts)
        }
        
        checkBalance()
    }
    
    func checkBalance () {
        guard let defaults = UserDefaults(suiteName: suiteName) else { return }
        balance = defaults.double(forKey: UserDefaultsKeys.balance.rawValue)
    }
    
    func deposit(value: Double) {
        balance += value
    }
    
    func withdraw(value: Double) {
        balance -= value
    }
}
