//
//  ShortcutManager.swift
//  Siri Bucks
//
//  Created by Bohdan Korda on 31.08.2020.
//  Copyright © 2020 Bohdan Korda. All rights reserved.
//

import UIKit
import Intents

@available(iOS 12.0, *)
public final class ShortcutManager {
    // MARK: Properties
    
    /// A shared shortcut manager.
    public static let shared = ShortcutManager()
    
    func donate(_ intent: INIntent, id: String? = nil) {
        // create a Siri interaction from our intent
        let interaction = INInteraction(intent: intent, response: nil)
        if let id = id {
            interaction.groupIdentifier = id
        }
        
        // donate it to the system
        interaction.donate { error in
            // if there was an error, print it out
            if let error = error {
                print(error)
            }
        }
        
        if let shortcut = INShortcut(intent: intent) {
            let relevantShortcut = INRelevantShortcut(shortcut: shortcut)
            INRelevantShortcutStore.default.setRelevantShortcuts([relevantShortcut]) { error in
                if let error = error {
                    print("Error setting relevant shortcuts: \(error)")
                }
            }
        }
    }
    
    /**
     This enum specifies the different intents available in our app and their various properties for the `INIntent`.
     Replace this with your own shortcuts.
     */
    public enum Shortcut {
        case whereIsMoney
        
        var defaultsKey: String {
            switch self {
            case .whereIsMoney: return "yourIntentShortcut"
            }
        }
        
        var intent: INIntent {
            var intent: INIntent
            switch self {
            case .whereIsMoney:
                intent = WhereIsMyMoneyIntent()
            }
            intent.suggestedInvocationPhrase = suggestedInvocationPhrase
            return intent
        }
        
        var suggestedInvocationPhrase: String {
            switch self {
            case .whereIsMoney: return "Где деньги?"
            }
        }
        
        var formattedString: String {
            switch self {
            case .whereIsMoney: return "Где деньги?"
            }
        }
        
        func donate() {
            // create a Siri interaction from our intent
            let interaction = INInteraction(intent: self.intent, response: nil)
            
            // donate it to the system
            interaction.donate { error in
                // if there was an error, print it out
                if let error = error {
                    print(error)
                }
            }
            
            
            if let shortcut = INShortcut(intent: intent) {
                let relevantShortcut = INRelevantShortcut(shortcut: shortcut)
                INRelevantShortcutStore.default.setRelevantShortcuts([relevantShortcut]) { error in
                    if let error = error {
                        print("Error setting relevant shortcuts: \(error)")
                    }
                }
            }
            
        }
    }
}
